#include <stdlib.h>

#include "binary_tree.h"

struct bt_node_t* bt_node_create(bt_value_t value,
                                 struct bt_node_t* left,
                                 struct bt_node_t* right) {
    // TODO
    return NULL;
}

void bt_node_destroy(struct bt_node_t* node) {
    // TODO
}

void bt_node_dump_graphviz(struct bt_node_t* node, FILE* file) {
    // to describe node use format
    // node_%pointer [label=\" value \"];
    //
    // to describe relation use format
    // node_%pointer_1 -> node_%pointer_2;
}

void bt_dump_graphviz(struct bt_node_t* root, FILE* file) {
    fprintf(file, "digraph G {\n");
    bt_node_dump_graphviz(root, file);
    fprintf(file, "}");
}

void bt_store_binary(struct bt_node_t* root, FILE* file) {
    // TODO
}

void bt_read_binary(FILE* file) {
    // TODO
}
