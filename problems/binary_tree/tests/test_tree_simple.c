
#include "../src/binary_tree.h"

enum {
    TEST_SUCCEEDED = 0,
    TEST_FAILED,
} TEST_RESULT;

int test_create() {
    /***************
            0
           / \
          1   3
         /   / \
        2   4   6
             \
              5
    ****************/
    struct bt_node_t* root =
        bt_node_create(
            0,
            bt_node_create(
                1,
                bt_node_create(2, NULL, NULL),
                NULL
            ),
            bt_node_create(
                3,
                bt_node_create(
                    4,
                    NULL,
                    bt_node_create(5, NULL, NULL)
                ),
                bt_node_create(6, NULL, NULL)
            )
        );

    bt_node_destroy(root);
    return TEST_SUCCEEDED;
}

int main() {
    int num_failed = 0;
    num_failed += test_create();
    printf("test: %s\n", (num_failed != 0) ? "FAIL" : "OK");
    return 0;
}