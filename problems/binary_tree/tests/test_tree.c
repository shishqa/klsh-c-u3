#include <check.h>

#include "../src/binary_tree.h"

enum {
    TEST_SUCCEEDED = 0,
    TEST_FAILED,
} TEST_RESULT;

START_TEST (create_node)
{
    const int test_value = 21;

    struct bt_node_t l = {};
    struct bt_node_t r = {};

    struct bt_node_t* node = bt_node_create(test_value, &l, &r);

    ck_assert_ptr_ne(node, NULL);
    ck_assert_int_eq(node->value, test_value);
    ck_assert_ptr_eq(node->left, &l);
    ck_assert_ptr_eq(node->right, &r);
}

START_TEST (create_tree)
{
    /***************
            0
           / \
          1   3
         /   / \
        2   4   6
             \
              5
    ****************/
    struct bt_node_t* root =
        bt_node_create(
            0,
            bt_node_create(
                1,
                bt_node_create(2, NULL, NULL),
                NULL
            ),
            bt_node_create(
                3,
                bt_node_create(
                    4,
                    NULL,
                    bt_node_create(5, NULL, NULL)
                ),
                bt_node_create(6, NULL, NULL)
            )
        );

    struct bt_node_t* test_node = root;
    ck_assert_int_eq(test_node->value, 0);
    ck_assert_ptr_ne(test_node->left, NULL);
    ck_assert_ptr_ne(test_node->right, NULL);

    test_node = root->left;
    ck_assert_int_eq(test_node->value, 1);
    ck_assert_ptr_ne(test_node->left, NULL);
    ck_assert_ptr_eq(test_node->right, NULL);

    test_node = root->left->left;
    ck_assert_int_eq(test_node->value, 2);
    ck_assert_ptr_eq(test_node->left, NULL);
    ck_assert_ptr_eq(test_node->right, NULL);

    test_node = root->right;
    ck_assert_int_eq(test_node->value, 3);
    ck_assert_ptr_ne(test_node->left, NULL);
    ck_assert_ptr_ne(test_node->right, NULL);

    test_node = root->right->left;
    ck_assert_int_eq(test_node->value, 4);
    ck_assert_ptr_eq(test_node->left, NULL);
    ck_assert_ptr_ne(test_node->right, NULL);

    test_node = root->right->left->right;
    ck_assert_int_eq(test_node->value, 5);
    ck_assert_ptr_eq(test_node->left, NULL);
    ck_assert_ptr_eq(test_node->right, NULL);

    test_node = root->right->right;
    ck_assert_int_eq(test_node->value, 6);
    ck_assert_ptr_eq(test_node->left, NULL);
    ck_assert_ptr_eq(test_node->right, NULL);

    bt_node_destroy(root);
    ck_assert_ptr_eq(root->left, NULL);
    ck_assert_ptr_eq(root->right, NULL);
}

Suite* tree_suite() {
    Suite* s = suite_create("binary tree");
    TCase* tc = tcase_create("all");

    tcase_add_test(tc, create_node);
    tcase_add_test(tc, create_tree);
    suite_add_tcase(s, tc);

    return s;
}

int main() {
    Suite* s = tree_suite();
    SRunner* runner = srunner_create(s);

    srunner_run_all(runner, CK_NORMAL);
    size_t n_failed = srunner_ntests_failed(runner);

    srunner_free(runner);

    return (n_failed == 0) ? 0 : 1;
}